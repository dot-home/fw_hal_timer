//********************************************************************************
/*!
\author     Kraemer Eduard
\date       05.04.2021

\file       HAL_Timer.c
\brief      Initialize the system timer. When an ARM-Core is available use the
            ARM-System Timer.

***********************************************************************************/
#include "TargetConfig.h"
#ifdef ESPRESSIF_ESP8266

#include "HAL_Timer.h"
#include "user_interface.h"

#define SYS_TICK_INTERVAL   1   //1ms intervall

static bool bSystemTimerRunning = false;
static os_timer_t SoftwareTimer;
static u32 ulTickCount = 0;
static pFunction pFnTimerCallback = NULL;

//********************************************************************************
/*!
\author     Kraemer E
\date       05.04.2021
\brief      Handles the system tick IRQ.
\return     none
\param      pArg - Arguments from the Timer
***********************************************************************************/
static void TimerIRQ(void* pArg)
{
    ulTickCount++;

    if(pFnTimerCallback)
    {
        pFnTimerCallback();
    }
}

//********************************************************************************
/*!
\author     Kraemer E
\date       05.04.2021
\brief      Sets the callback function for the interrupt
\return     eReturn - Enumeration 
\param      pFnCallback - Function pointer to the callback function.
***********************************************************************************/
teSysTickReturn HAL_Timer_OverwriteCallback (pFunction pFnCallback)
{
    teSysTickReturn eReturn = eSysTick_Success;
    
    os_timer_disarm(&SoftwareTimer);

    u32 ulTimerPeriod = SoftwareTimer.timer_period;

    os_timer_setfn(&SoftwareTimer, (ETSTimerFunc*)pFnCallback, NULL);
    os_timer_arm(&SoftwareTimer, ulTimerPeriod, true);
    
    return eReturn;
}

//********************************************************************************
/*!
\author     Kraemer E
\date       05.04.2021
\brief      Changes the reload time of the system tick timer
\return     eReturn - Enumeration 
\param      ulReloadTime - The time between the interrupts shall be generated
***********************************************************************************/
teSysTickReturn HAL_Timer_SetReload(u32 ulReloadTimeMs)
{
    teSysTickReturn eReturn = eSysTick_Success;
    
    os_timer_disarm(&SoftwareTimer);

    os_timer_arm(&SoftwareTimer, ulReloadTimeMs, true);
    
    return eReturn;
}


//********************************************************************************
/*!
\author     Kraemer E
\date       05.04.2021
\brief      Initializes the system timer
\return     eReturn - Enumeration 
\param      pFnCallback - Function pointer to the callback function which shall be
                          called on every system tick.
***********************************************************************************/
teSysTickReturn HAL_Timer_Init(pFunction pFnCallback)
{
    teSysTickReturn eReturn = eSysTick_Success;
    
    pFnTimerCallback = pFnCallback;

    /* Set timer interrupt function */
    os_timer_setfn(&SoftwareTimer, TimerIRQ, NULL);

    /* Enable timer */
    /* os_timer_arm -  Enable a millisecond granularity timer.    
    void os_timer_arm(os_timer_t *pTimer, uint32_t milliseconds, bool repeat)    
    Arm a timer such that is starts ticking and fires when the clock reaches zero.    
      - pTimer parameter is a pointed to a timer control structure.
      - milliseconds parameter is the duration of the timer measured in milliseconds. 
      - repeat parameter is whether or not the timer will restart once it has reached zero. */    
    os_timer_arm(&SoftwareTimer, SYS_TICK_INTERVAL, true);

    bSystemTimerRunning = true;

    return eReturn;
}

//********************************************************************************
/*!
\author     Kraemer E
\date       05.04.2021
\brief      Stops the sys tick timer and deletes the callbacks functions
\return     eReturn - Enumeration 
\param      none
***********************************************************************************/
teSysTickReturn HAL_Timer_Stop(void)
{
    teSysTickReturn eReturn = eSysTick_Success;
    
    os_timer_disarm(&SoftwareTimer);
    bSystemTimerRunning = false;
    return eReturn;
}


//********************************************************************************
/*!
\author     Kraemer E
\date       14.05.2021
\brief      Returns the status of the system tick timer.
\return     none
\param      bSystemTimerRunning - True when timer is running otherwise false
***********************************************************************************/
bool HAL_Timer_GetTimerStatus(void)
{
    return bSystemTimerRunning;
}

//********************************************************************************
/*!
\author     Kraemer E
\date       05.04.2021
\brief      Stops the sys tick timer and deletes the callbacks functions
\return     ulTick - Tick count of the used system timer 
\param      none
***********************************************************************************/
u32 HAL_Timer_GetTickCount(void)
{   
    return ulTickCount;
}

#endif //ESPRESSIF_ESP8266