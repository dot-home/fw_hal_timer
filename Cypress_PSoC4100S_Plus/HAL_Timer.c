//********************************************************************************
/*!
\author     Kraemer Eduard
\date       05.04.2021

\file       HAL_Timer.c
\brief      Initialize the system timer. When an ARM-Core is available use the
            ARM-System Timer.

***********************************************************************************/
#include "TargetConfig.h"
#ifdef CYPRESS_PSOC4100S_PLUS

#include <project.h>

#include "HAL_Timer.h"

#define SYS_CLK_FREQ        CYDEV_BCLK__SYSCLK__HZ          //Frequency in Hertz
#define SYS_TICK_CALLBACKS  CY_SYS_SYST_NUM_OF_CALLBACKS    //Amount of system tick callbacks
#define SYS_TICK_MAX_VALUE  CY_SYS_SYST_RVR_CNT_MASK        //Max reload value which can be set

static bool bSystemTimerRunning = false;

//********************************************************************************
/*!
\author     Kraemer E
\date       05.04.2021
\brief      Sets the callback function for the interrupt
\return     eReturn - Enumeration 
\param      pFnCallback - Function pointer to the callback function.
***********************************************************************************/
teSysTickReturn HAL_Timer_OverwriteCallback (pFunction pFnCallback)
{
    teSysTickReturn eReturn = eSysTick_InvalidPointer;
    
    #ifdef __arm__
        if(pFnCallback)
        {
            /* Find the currently used callback slot */
            for(u8 ucCallbackEntry = 0; ucCallbackEntry < SYS_TICK_CALLBACKS; ucCallbackEntry++)
            {
                /* Search for used entry and overwrite it */
                if(CySysTickGetCallback(ucCallbackEntry) != NULL)
                {
                    CySysTickSetCallback(ucCallbackEntry, pFnCallback);
                    eReturn = eSysTick_Success;
                    break;
                }
            }
        }
    #else
        #warning Set up another timer
    #endif
    
    return eReturn;
}

//********************************************************************************
/*!
\author     Kraemer E
\date       05.04.2021
\brief      Changes the reload time of the system tick timer
\return     eReturn - Enumeration 
\param      ulReloadTime - The time between the interrupts shall be generated
***********************************************************************************/
teSysTickReturn HAL_Timer_SetReload(u32 ulReloadTimeMs)
{
    teSysTickReturn eReturn = eSysTick_InvalidReload;
    
    #ifdef __arm__       
        
        /* Formula to get the reload time: Value = (Frequency (Hz) * Time (ms)) / FactorMilliSecond */
        u32 ulReloadTime = (SYS_CLK_FREQ * ulReloadTimeMs)/1000;
                
        if(ulReloadTime && ulReloadTime < SYS_TICK_MAX_VALUE)
        {
            /* Configure sys tick timer to generate interrupts */
            if(CySysTickGetReload() != ulReloadTime)
            {
                CySysTickSetReload(ulReloadTime);
                CySysTickClear();
            }
            
            eReturn = eSysTick_Success;
        }
    #else
        #warning Set up another timer
    #endif
    
    return eReturn;
}


//********************************************************************************
/*!
\author     Kraemer E
\date       05.04.2021
\brief      Initializes the system timer
\return     eReturn - Enumeration 
\param      pFnCallback - Function pointer to the callback function which shall be
                          called on every system tick.
***********************************************************************************/
teSysTickReturn HAL_Timer_Init(pFunction pFnCallback)
{
    teSysTickReturn eReturn = eSysTick_InvalidPointer;
    
    #ifdef __arm__
                
        /* Configure the SysTickTimer to generate every 1ms an interrupt and starts 
           the operation. */
        CySysTickStart();
                
        /* Find an empty callback slot and assign the given callback function to it */
        if(pFnCallback)
        {
            for(u8 ucCallbackEntry = 0; ucCallbackEntry < SYS_TICK_CALLBACKS; ucCallbackEntry++)
            {
                if(CySysTickGetCallback(ucCallbackEntry) == NULL)
                {
                    /* Set callback */
                    CySysTickSetCallback(ucCallbackEntry, pFnCallback);
                    bSystemTimerRunning = true;
                    eReturn = eSysTick_Success;
                    break;
                }
            }
        }        
    #else
        #warning Set up another timer
    #endif
    
    return eReturn;
}

//********************************************************************************
/*!
\author     Kraemer E
\date       05.04.2021
\brief      Stops the sys tick timer and deletes the callbacks functions
\return     eReturn - Enumeration 
\param      none
***********************************************************************************/
teSysTickReturn HAL_Timer_Stop(void)
{
    teSysTickReturn eReturn = eSysTick_InvalidPointer;
    
    #ifdef __arm__
        /* Stops the sys tick timer */
        CySysTickStop();
                
        /* Find an used callback slot and delete the callback function by overwriting with NULL */
        for(u8 ucCallbackEntry = 0; ucCallbackEntry < SYS_TICK_CALLBACKS; ucCallbackEntry++)
        {
            if(CySysTickGetCallback(ucCallbackEntry) != NULL)
            {
                /* Set callback */
                CySysTickSetCallback(ucCallbackEntry, NULL);
                bSystemTimerRunning = false;
                eReturn = eSysTick_Success;
                break;
            }
        }        
    #else
        #warning Set up another timer
    #endif
    
    return eReturn;
}


//********************************************************************************
/*!
\author     Kraemer E
\date       14.05.2021
\brief      Returns the status of the system tick timer.
\return     none
\param      bSystemTimerRunning - True when timer is running otherwise false
***********************************************************************************/
bool HAL_Timer_GetTimerStatus(void)
{
    return bSystemTimerRunning;
}

//********************************************************************************
/*!
\author     Kraemer E
\date       05.04.2021
\brief      Stops the sys tick timer and deletes the callbacks functions
\return     ulTick - Tick count of the used system timer 
\param      none
***********************************************************************************/
u32 HAL_Timer_GetTickCount(void)
{   
    #ifdef __arm__
        u32 ulTick = CySysTickGetValue();
    #else
        #warning Set up another timer
    #endif
    
    return ulTick;
}

#endif //PSOC_4100S_PLUS