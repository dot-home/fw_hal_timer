//********************************************************************************
/*!
\author     Kraemer Eduard
\date       05.04.2021

\file       HAL_TIMER.h
\brief      Interface for the system timer

***********************************************************************************/


#ifndef _HAL_TIMER_H_
#define _HAL_TIMER_H_

#include "BaseTypes.h"
    
#ifdef __cplusplus
extern "C"
{
#endif

typedef enum
{
    eSysTick_Success,           /* Operation was successful */
    eSysTick_InvalidPointer,    /* Given pointer is invalid */
    eSysTick_InvalidReload,     /* Reload value is invalid */
    eSysTick_Invalid            /* Generic invalid */
}teSysTickReturn;


teSysTickReturn HAL_Timer_OverwriteCallback(pFunction pFnCallback);
teSysTickReturn HAL_Timer_Init(pFunction pFnCallback);
teSysTickReturn HAL_Timer_SetReload(u32 ulReloadTimeMs);
teSysTickReturn HAL_Timer_Stop(void);
u32 HAL_Timer_GetTickCount(void);
bool HAL_Timer_GetTimerStatus(void);

#ifdef __cplusplus
}
#endif

#endif // _HAL_TIMER_H_
